# Editing Kit (Creative Freedom Summit 2024)

Welcome, fellow video editor! We'd love to present you the Kdenlive project with pre-made motion graphics, samples and guidelines, serving as a template for a video montage of Creative Freedom Summit talks. This manual is going into details of how to use each sample, how to line it up with the rest of the footage and what terms you need to know while working with the project.

With recent merge request, there are also files suitable for use in Shotcut and other video editing applications (planning to take over).

If you're having trouble with something, proceed with the [further notes](#further-notes).

---

---

## Requirements

- Kdenlive 23.08.4+ or another video editor
- [Montserrat](https://fonts.google.com/specimen/Montserrat) font (styles used: Regular 400, SemiBold 600)

## Terminology

Non-linear video editing in Kdenlive and our advanced kit bring their luggage of terms to familiarize with.

### Sequences

Sequences are the recent feature of Kdenlive. They allow us to non-destructively group small details and to reuse same segments. Internally, it can consist of many tracks. Externally, it's a single pair of video and audio!

Imagine sequence as a box: you don't have to see all the nitty-gritty, while making sure every tiny piece remains inside, when reviewing the project's entire timeline. From one fellow editor to another, it is an extremely useful tool, as it lets us scatter such boxes and only get back to them as they are requested.

This kit employs an extensive use of **sequences** for every animated piece, ready to be dropped onto the timeline. How exactly you apply them, however, differs from case to case.

There are two **main sequences** in the kit: `FINAL` and `SAMPLES`. For your edits, use the `FINAL` sequence. The `SAMPLES` provide an overview of other small sequences, how you could use them together with titles and other things, what transitions to use throughout the video. Such examples are not set in stone, but they allow our videos to have the sense of commonality, so try to stay consistent with the segments provided. You can copy and paste contents from the `SAMPLES` onto the `FINAL`.

### Tracks

Tracks are a part of the timeline, now attributed to the sequence. They allow you to compose the video out of multiple pieces. Two regular types of tracks are **video** and **audio**.

Our main sequences have the same layout: 5 video (`V1-V5`) and 5 audio tracks (`A1-A5`). They are mirrored, in a way, to let you drop things without worrying about audio from sequences consuming an entire space. This timeline setup should help you get started with editing any talk, while also presenting some high quality imagery. If you don't use some tracks, simply collapse them.

#### Base Track

Purpose of the `Base` track varies on circumstances that each segment presents: fullscreen parts of the footage, slides, backgrounds. There are cases when footage is missing, and so it's the very track where you may find yourself dropping placeholders/backgrounds onto.

#### Lower Overlay

Sometimes there are things that the original footage cannot cover: it's pointing at the answer. There are only so much things that speaker can improvise with, but video editor can add their own **flair**, helpful visual markers for the viewer on the `Lower Overlay` track. It may also be used to put moving pictures that should never cover the speaker's face (that's why it's called "lower").

#### Speaker

`Speaker` track may come in handy for placing a **democam** on top of the `Base` and `Lower Overlay`. Then, supporting audio track will probably be a focal point of cutting out any stutters. When in use, make sure that `Base: Audio` track is muted or used in the rare case.

#### Top Overlay

Almost the highest track. `Top Overlay` is a good place for cards and supers.

#### Topmost Overlay

The highest track. `Topmost Overlay` can be of use for titles and anything that must supercede the layer just above the `Speaker`.

#### Custom tracks

Add as many new tracks as you require. But before you do - maybe think in terms of child sequences. It is easy to manage tracks **inside a short sequence**, but not for the entirety of the talk. If there are just so many objects, you can always lay things on each other within a sequence separate from the `FINAL`, then nest it.

### Supers

Super is another name for a "lower third". In our case, lower third is up above, hence the other name. Editor is encouraged to use three types of supers throughout the video: audience **questions**, notable **tips**, and **link** reminders.

### Democam

Democam is a demonstration camera: basically, speaker's face camera, but of a consistently small size. With footage provided, we have a fixed user interface of Jitsi with a bunch of small cameras/avatars on the side, while speaker has their screen shared. As a rule, there are never more than 5 concurrent **slots**, one of which would be the speaker showing their facial expressions. Democam would be exactly the term for an extracted slot where only one person is visible.

---

---

## Intro Card

The start of every CFS talk: a card with the same design, but corresponding title and speaker's name.

### Replace intro card image

1. Locate the intro card image in Project Bin (search for `REPLACE ME`).
2. Open menu and select `Replace Clip...` item.
3. Pick a valid image file with your talk's introduction card.

### What comes after the intro card

Right at `00:00:06:13` timestamp, intro card transitions into the presentation of talk. It usually includes a face camera of event organizer, speaker, or screen with the first slide.

In case when video shall not start with anything big to show, use the pre-made sample that shows how `Intro Card` can neatly transform into `Placeholder Card 1`. Incidentally, the `FINAL` sequence has a convenient timeline setup already.

Placeholder card can then act as a filler. Though, since the first few seconds of the talk are an apt time to capture viewer's focus, it should provide something on top of said background: a brief illustration of the topic at hand or short precis. Again, there is no need for any of this if speaker has already provided a **good start**.

## Closing Remarks

If the speaker has final words worth a chapter on its own, feel free to add some flair and even a music in the background (`airtone - commonGround`).

## Contacts

If there aren't much contacts on the slide, because it turns out the speaker wanted to include **more ways to contact** them, more such data can be put in the `Contacts(*): Sequence`.

Few pre-downloaded 96x96 icons from [Remix Icon](https://remixicon.com/) pack are available just for this use case: `at`, `mail`, `global`, `mastodon`, `chat`. Each can be put on the timeline as an entry on itself to quickly communicate which social platform associates with the **speaker's profile**.

### How to add contacts

1. Inside the `SAMPLES` sequence, locate the child `Contacts (Odd): Sequence`.
2. Select the clips starting from `V1` track to `V4`. This should include the Placeholder Card, Avatar, and Contacts.
3. Copy and paste them onto the `FINAL`.
4. Replace avatar clip by proceeding to the Project Bin. Alternatively, you can reuse speaker's face camera and reapply the same effects by dragging them from the sample avatar.
5. Replace `Contacts (Odd): Sequence` with an "even" counterpart, if there are exactly 2 or 4 entries in the list.
6. Double click the Contacts sequence to start editing it.
7. Use `at` entry to mark the speaker's handle on any controversial (as a rule, popular and centralized) platforms.
8. Remove trailing entries that you don't need (one at the top and the bottom).
9. Replace entry icons, while carrying the same effects. Last `Position/Zoom` effect sets up a vertical position for the entry.
10. Replace text on each entry.
11. Fix the timings for entries appearing, synchronize with the speaker mentioning them or distribute randomly. Re-check how it looks in the `FINAL` sequence.
12. Ensure that `Link` sound plays for each entry appearing, to stay consistent and provide an audio feedback.

## Outro Card

The end of every CFS talk with nothing but Summit's attributes and music credit.

### Add outro card after the video is done

1. Find a sample with outro card inside the `SAMPLES` sequence.
2. Copy and paste it onto the `FINAL`, `Top Overlay` track.
3. Align it with ending of the talk, using the accomodating placeholder card to match timings. Correct alignment should warrant a nice fade transition from presentation of the talk to the outro card.
4. Remove or reuse placeholder card for the transition.

---

---

## Using democams

If entirety of your video doesn't go over the presentation at fullscreen, democams serve no purpose there. But if it happens to do just that, then it's a good way to show everything at scale and leave **no distractions**, while securing the presence of the speaker throughout the video. It's an experimental method of achieving basically what could be done with Jitsi itself, if it was recorded with UI changes. Alas, we've got the footage "as is", and post-processing with the method like democams is the only thing left to do.

### Speaker is The Chosen One

1. Looking at the original footage, determine the **slot** in which the speaker resides (1-5 from bottom to top).
2. From the `SAMPLES`, locate the democams and pick the clip where only the right slot is visible.
3. Right click and `Copy` it.
4. Right click the clip on your `FINAL` sequence and choose `Paste effects`. Now it should only show the speaker in the right bottom corner.

### Speaker changed slots?

1. In the place where swap happened, cut the clip and remove effects from the righthand clip.
2. Perform the same set of instructions as above.

## Q&A

Questions and answers section usually comes after the speaker has given their speech. In any case, if any **question from the audience** comes up, use the **question super** to display it on the screen.

### Add a question super

1. Import MOV `kit/supers/cfs-super-question.mov`, if it's not in the Project Bin already.
2. Drop it onto the `FINAL`, track `V4` or `V5`.
3. Align the start of super with the vocal read of the question.
4. Locate the demo `kit/supers/cfs-2024-super-demo.kdenlive` and borrow the `Text` clip for use in your project.
5. Paraphrase the question enough to fit it into one or two lines, type it in external text editor and copy into clipboard.
6. Proceed to `Text` clip, inspect `Effects`  to find the `Dynamic Text` effect.
7. Locate the text input for your question, clear and paste the clipboard contents.
8. Adjust the duration of both the MOV and `Text` clip to let the viewer be up to speed. Neutral default: 8s.
9. Add both fade in and fade out with 1s duration on both ends of `Text` clip.
10. Add fade out with 1s duration on the rear end of MOV clip.

## Notable Tips

There are times when speaker may stress a **certain point** and its priority is not immediatelly obvious. This is a good use for **tip super**! Don't overuse those, though.

Instructions for adding this super are very similar to how you add a question.

## Links

While a video description is useful for easily accessible links, you really need to **bind everything together**. When the speaker says there is some material they would like to share or it's discovered upon reading the live chat, don't forget to make a notice in the video itself with the help of **link super**.

Instructions for adding this super are very similar to how you add a question.

---

---

## Further notes

**Dear video editor!** Thank you for reading this and taking part in Creative Freedom Summit. As with any soft guidelines, Editing Kit and manual included with it are meant to dispel any doubts and technical barriers that seem hard to overcome at first. These are not set-in-stone rules, so following them to a T is only making things way too perfect than they have to be. We shall apply the principles of creative freedom to any videos that come our way and gladly accept the edits with enough substance.

If you have questions left in relation to this kit or encounter technical hurdles in Kdenlive, please direct them to the issue tracker or [#creativefreedom:fedora.im](https://matrix.to/#/#creativefreedom:fedora.im).

## License

This work is marked with [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/).

### Third party licenses

- Cards are [courtesy](https://gitlab.com/fedora/design/team/requests/-/issues/19) of @jessicachitas and other contributors
- Remix Icon - [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
- "commonGround" by airtone - [Attribution-NonCommercial 3.0 Unported](https://creativecommons.org/licenses/by-nc/3.0/)
